###########################################################
### EC2 security group
###########################################################

module "git_runner-sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.17.0"

  name        = "git_runner-sg"
  description = "ssh incoming and open outgoing trafic"
  vpc_id      = module.vpc_git_runner_east_1.vpc_id

  ingress_cidr_blocks = [var.public_ssh_ip]
  ingress_rules       = ["ssh-tcp"]

  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  providers = {
    aws = aws.east-1
  }

  depends_on = [
    module.vpc_git_runner_east_1
  ]
}
