
resource "aws_iam_role" "runner_role" {
  name = "runner_role"
  provider   = aws.east-1

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
      tag-key = "tag-value"
  }
}

resource "aws_iam_instance_profile" "runner_profile" {
  name = "runner_profile"
  provider   = aws.east-1
  role = aws_iam_role.runner_role.name
}

resource "aws_iam_role_policy" "runner_policy" {
  name = "test_policy"
  provider   = aws.east-1
  role = aws_iam_role.runner_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*", 
        "dynamodb:*",
        "ec2:*",
        "acm:*",
        "elasticloadbalancing:*",
        "ssm:Describe*",
        "ssm:Get*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}