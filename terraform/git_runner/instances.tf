######################################################################
# create key-pair (in runner) for logging to the ec2

resource "aws_key_pair" "key_east_1" {
  provider   = aws.east-1
  key_name   = "key_east_1"
  public_key = file("~/.ssh/id_rsa.pub")
}

###########################################################
### EC2 instabce modules
###########################################################

module "gitlab-runner-east-1" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.16.0"

  name           = "gitlab-runner-east-1"
  instance_count = var.gitlab-runner-count

  ami                    = data.aws_ssm_parameter.linux_ami_east_1.value
  instance_type          = var.instance-type
  key_name               = aws_key_pair.key_east_1.key_name
  monitoring             = true
  vpc_security_group_ids = [module.git_runner-sg.this_security_group_id]
  subnet_id              = element(module.vpc_git_runner_east_1.public_subnets, 0)
  iam_instance_profile   = aws_iam_instance_profile.runner_profile.name

  providers = {
    aws = aws.east-1
  }

  tags = {
    Name = "gitlab-runner-east-1"
  }
}

