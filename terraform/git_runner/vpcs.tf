###########################################################
### VPC modules
###########################################################
module "vpc_git_runner_east_1" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.64.0"

  name     = "vpc_git_runner_east_1"
  cidr     = var.vpc_git_runner_east_1_cidr

  azs            = slice(data.aws_availability_zones.azs_east_1.names, 0, 2)
  public_subnets = var.public_subnet_east_1
  #private_subnets = 
  #create_database_subnet_group = false
  #nat gateway for private subnets
  #enable_nat_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true

   providers = {
    aws = aws.east-1
  }
  
  tags = {
    Name        = "vpc_git_runner"
    Environment = "infra"
  }
}
