######################################################################
# define provider for infra credential and two regions

provider "aws" {
  version = "~>2.0"
  region  = element(var.region, 0)
  profile = element(var.profile, 0)
  alias   = "east-1"
}

provider "aws" {
  version = "~>2.0"
  region  = element(var.region, 1)
  profile = element(var.profile, 0)
  alias   = "west-2"
}