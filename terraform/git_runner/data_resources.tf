#####################################################################
# get list of available AZ in each region

data "aws_availability_zones" "azs_east_1" {
  provider = aws.east-1
}

######################################################################
# get linux AMI ID ussing SSM parameter endpoint
data "aws_ssm_parameter" "linux_ami_east_1" {
  provider = aws.east-1
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}