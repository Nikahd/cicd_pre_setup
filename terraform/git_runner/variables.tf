#####################################################################
# region and profile

variable "region" {
  type    = list(string)
  default = ["us-east-1", "us-west-2"]
}

variable "profile" {
  type    = list(string)
  default = ["infra", "dev", "temp"]
}

#####################################################################
# vpcs cidr names

variable "vpc_git_runner_east_1_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

#####################################################################
# subnets name

variable "public_subnet_east_1" {
  type    = list(string)
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

#####################################################################
# permited ip for public ssh

variable "public_ssh_ip" {
  type    = string
  default = "0.0.0.0/0"
}

#####################################################################
# ec2 instances type & count

variable "instance-type" {
  type    = string
  default = "t2.micro"
}

variable "gitlab-runner-count" {
  type    = number
  default = 1
}