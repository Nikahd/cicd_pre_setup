
output "s3_buckeet" {
  value = aws_s3_bucket.state_bucket.bucket
}

output "dynamodb" {
  value = aws_dynamodb_table.terraform_statelock.name
}