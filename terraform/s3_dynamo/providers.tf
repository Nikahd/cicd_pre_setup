
provider "aws" {
  version = "~>2.0"
  region  = var.region
  profile = element(var.profile, 0)
}