variable "region" {
  type    = string
  default = "us-east-1"
}

variable "profile" {
  type    = list(string)
  default = ["infra", "dev", "temp"]
}

variable "aws_bucket_prifix" {
  type    = string
  default = "globol-remote"
}

variable "aws_dybnamo_prifix" {
  type    = string
  default = "global-remote-table"
}

variable "full_access_users" {
  type    = list(string)
  default = ["infra_user"]
}
