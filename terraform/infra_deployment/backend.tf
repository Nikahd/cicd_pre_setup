#####################################################################
# move the terraform state file to AWS S3 and dynamodb

terraform {
  backend "s3" {
    bucket         = "globol-remote-13392"
    key            = "infra/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "global-remote-table-13392"
  }
}