#####################################################################
# region and profile

variable "region" {
  type    = list(string)
  default = ["us-east-1", "us-west-2"]
}

variable "profile" {
  type    = list(string)
  default = ["infra", "dev", "temp"]
}

#####################################################################
# vpcs cidr names

variable "vpc_test_west_2_cidr" {
  type    = string
  default = "192.168.0.0/16"
}

#####################################################################
# subnets name

variable "public_subnet_west_2" {
  type    = list(string)
  default = ["192.168.1.0/24", "192.168.2.0/24"]
}

#####################################################################
# permited ips for public ssh

variable "public_ssh_ip" {
  type    = string
  default = "0.0.0.0/0"
}

#####################################################################
# ec2 instances type & count

variable "instance-type" {
  type    = string
  default = "t2.micro"
}

variable "ec2-test-count" {
  type    = number
  default = 1
}