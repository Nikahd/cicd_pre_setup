#####################################################################
# get list of available AZ in each region

data "aws_availability_zones" "azs_west_2" {
  provider = aws.west-2
}

######################################################################
# get linux AMI ID ussing SSM parameter endpoint
data "aws_ssm_parameter" "linux_ami_west_2" {
  provider = aws.west-2
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}