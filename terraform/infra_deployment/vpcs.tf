###########################################################
### VPC module
###########################################################
module "vpc_git_runner_west_2" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.64.0"

  name     = "vpc_git_runner_west_2"
  cidr     = var.vpc_test_west_2_cidr

  azs            = slice(data.aws_availability_zones.azs_west_2.names, 0, 2)
  public_subnets = var.public_subnet_west_2

  enable_dns_hostnames = true
  enable_dns_support   = true

   providers = {
    aws = aws.west-2
  }
  
  tags = {
    Name        = "vpc_test"
    Environment = "dev"
  }
}
