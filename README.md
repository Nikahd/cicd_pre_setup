This project is about provisioning and preparing a CI/CD enviromnet and all automtion prerequisites in order to following IaS approach for any resource provisoning into AWS.

At the end of this deployment we will have eigher local or cloud GitLab as a version managment repository to keep all the provisioning codes (Terraform). This server is also included a pipeline with the intention of continuous integration and continuous delivery. Therefore, we need a GitLab Runner (worker) to push the codes. These Runners will be sit in AWS. Accordingly, we need to have a dedicated account for deplyoing these Runners. Then we can ceate other accounts for developnment team (like, non-prod, stage prod and ...). Finally to make these Runners able to push the deployment to other accounts, we should define/ assign role in CICD account and consume it in other accounts. Finally, we need a central and safe location to put teraform state files. So, we use AWS S3 to keep the Terraform state files and DynamoDB  to keep terraform lucking.

The above statement will be broken down in these phases:
1. Provisioning S3 and DynamoDB.
2. Provisioning CI/CD environment and deplyoing GitLab Runners in this environment.
3. Provisioning AWS resources for development teams. 

**Provisioning S3 and DynamoDB**

For this phase we are using "terraform/s3_dynamo" in GitLab to keep the Terraform provisioning codes. Then we checkout his code in a local system and run the Terraform locally to deploy S3 and DynamoDB and at the end push back all the files to the GitLab to keep the Teraform state file in shareplace. Credential method for access to AWS is "AWS CLI profile" in local system.

Here is list of details:

1. create new project in GitLab server with name of "cicd_pre_setup"
2. update readme.md file
3. clone the repository to the local system:
    1. create a ssh key in the local system
    - ssh-keygen -t ed25519
    - cd .ssh/
    - ls
    - cat id_ed25519.pub
    2. add public key to the gitlab account
    - edit profile/ ssh keys /add key 
    4. clone the repositoyry to the local system
    - git clone git@gitlab.com:Nikahd/cicd_pre_setup.git

4. checkout new files (variables.tf, resources.tf, providers.tf, output.tf)to the local system:
    - cd cicd_pre_setup/
    - git checkout master         # switch to the master branch
    - git pull                    # pull down the latest changes
    - git checkout -b ticket1     # create and switch to a new feature branch based on the ticket number
    - git pull origin master      #pull down the latest from master origin to the branch
    - terraform intit
    - terraform plan
    - terraform apply
    - grabbing S3 bucket and DynamoDb table name
    - pushing back files to the GitLab 
    - adding new files:
    - git add .                                  # add the files to the ocal repository 
    - git commit -m "ticket1: ading new fikles"   # commit the changes
    - git push origin ticket1

5. Request merge from branch to master

**Provisioning CI/CD environment and deplyoing GitLab Runners in this environment.**

In this phase we deploy th Runners into AWS. Therefor we need to create VPC, AZs, public networks, EC2s, role and all other requiremtns. we configure terrafom in a way to save the state files / loucking state in S3/ DaynamoDB. We also need to configure a Runner to push the configuraion via GitLab pipeline. as the Runners in AWS are not ready yet, we use a local sytem as Runner. Here is a details of configuring local Runner:

    1. install gitlab runner on runner (mac):
    - brew install gitlab-runner          # To install GitLab Runner using Homebrew
    - brew services start gitlab-runner   # Install GitLab Runner as a service and start it
    2. Register Runner (mac):
    - sudo gitlab-runner register --url https://gitlab.com/ --registration-token U_FD-hUG9yNqbLcCHM68
    3. run runner on runner
    - sudo gitlab-runner run

We also need to have access to the EC2s in AWS for management purpose, so we create local private/ public key:

    ssh-keygen -t rsa

we should coppy the public key in Runner system as we want Runner copy this key to the EC2s. Here is the address we keep the public key in Runner:

    ~/.ssh/id_rsa.pub

Technically, we should use "gitlab-runner" module to create a runner in aws included role and other dependencies. However, for the sake of time limitaion we manually create an EC2 instance as a GitLab Runner. we manually make ssh to the EC2 and run the following commands:

    #ssh to the box:
    ssh -i "key_east_1.pem" ec2-user@ec2-3-216-23-84.compute-1.amazonaws.com

    #install GitLab ronner:
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
    export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E yum install gitlab-runner
    yum list gitlab-runner --showduplicates | sort -r
    export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E yum install gitlab-runner-10.0.0-1
    
    #register runner to the GitLab
    sudo gitlab-runner register

    #add aws credentials
    aws configure --profile infra

    #install Terraform
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
    sudo yum -y install terraform
    





